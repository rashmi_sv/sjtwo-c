#include "switch_led_logic.h"
#include "board_io.h"
#include "gpio.h"

// This function checks the value of board switch 3 and sets the value of LED 3 accordingly.
// If the switch is pressed, LED 3 turns OFF. If switch is released, LED 3 turns ON.
// This logic is called whenever the callback in which it is called gets executed.
// If the switch is pressed and released faster than the callback function calling rate, then
// there will not be any change in state of the LED.
void switch_led_logic__run_once() {
  if (gpio__get(board_io__get_sw3())) {
    gpio__set(board_io__get_led3());
  } else {
    gpio__reset(board_io__get_led3());
  }
}