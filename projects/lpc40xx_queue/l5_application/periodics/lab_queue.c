#include "lab_queue.h"

// This should initialize all members of queue_s
bool queue__init(queue_s *queue) {
  if (queue) {
    queue->head = 0;
    queue->tail = 0;
    queue->count = 0;
    return true;
  }
  return false;
}

bool queue__is_queue_empty(queue_s *queue) {
  if (queue && queue->count == 0) {
    return true;
  }
  return false;
}

bool queue__is_queue_full(queue_s *queue) {
  if (queue && queue->count == MAX_QUEUE_SIZE) {
    return true;
  }
  return false;
}

/// @returns false if the queue is full
bool queue__push(queue_s *queue, uint8_t push_value) {
  if (queue == NULL || queue__is_queue_full(queue)) {
    return false;
  }
  queue->queue_memory[queue->tail] = push_value;
  queue->tail = (queue->tail + 1) % MAX_QUEUE_SIZE; // to make sure that tail wraps around after reaching the limit
  queue->count++;
  return true;
}

/// @returns false if the queue was empty
bool queue__pop(queue_s *queue, uint8_t *pop_value) {
  if (queue == NULL || pop_value == NULL || queue__is_queue_empty(queue)) {

    return false;
  }
  *pop_value = queue->queue_memory[queue->head];
  queue->head = (queue->head + 1) % MAX_QUEUE_SIZE; // to make sure that head wraps around after reaching the limit
  queue->count--;
  return true;
}

bool queue__get_item_count(const queue_s *queue, size_t *count) {
  if (queue && count) {
    *count = queue->count;
    return true;
  }
  return false;
}